% SVN HOWTO
% O. Grunder
% 04/04/2016


# Install and Admin SVN+apache

    $ sudo apt-get install apache2 libapache2-mod-svn subversion
    $ sudo a2enmod dav_svn


 Creating passwd file with first user 

    $ sudo htpasswd -cs /etc/apache2/dav_svn.passwd user1
	$ sudo chown www-data:www-data /etc/apache2/dav_svn.passwd
	$ sudo /etc/init.d/apache2 restart


 Adding a user

    $ sudo htpasswd -s /etc/apache2/dav_svn.passwd user2
	$ sudo /etc/init.d/apache2 restart


 SVN config file to define projects config

    /etc/apache2/mods-enabled/dav_svn.conf
	$ sudo /etc/init.d/apache2 restart


 Creation of the initial project in the SVN database

    $ sudo svnadmin create /home/svn/myproject

	$ sudo chown -R www-data.www-data /home/svn/myproject

    $ svn import myproject_dir http://localhost/svn/myproject -m 'Initial import for ...'


 Deletion of a svn project

    svn delete -m "message" http://localhost/svn/myproject



# User

 Initial copy of the project from svn server 

    $ svn checkout http://localhost/svn/myproject

 Update the local copy from the server

    $ svn update

 Copy the local changes to the server

    $ svn commit 

 Creation of a snapshot/tags directory for different releases

    $ svn mkdir tags
    $ svn copy trunk/ tags/new-release-v01
    $ svn commit -m "new release" tags

  Remote snapshot

    $ svn copy http://localhost/svn/myproject http://localhost/svn/myproject/tags/release-0.3 -m "Tagging the 0.3 release of the project."


# Documentation

https://doc.ubuntu-fr.org/subversion#serveur\_svn\_apache





