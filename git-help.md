% GIT HOWTO
%
% 22/03/2019

# Description 

GIT  est un logiciel de gestion de version décentralisé qui stocke les versions
d'un projet informatique en local ($localrepo$) et sur un serveur distant
($remoterepo$). 5 types d'objets existent : 

- **blob** : binary large object pour les fichiers
- **tree** : arborescence de fichiers
- **commit** : arborescence de fichiers plus des métadonnées relatives à la
    version (date, auteur, ...)
- **tag** : étiquette permettant de répérer facilement un commit particulier
- **branch** : permettant de créer une variante d'un projet en parallèle du projet
    principal


# Commandes

## Création

1. Initialisation d'un espace de travail : `git clone`. 

Il y a 2 modes : http et ssh, mais ssh est le mieux. Pour gitlab par exemple, il
est préférable d'utiliser le mode ssh, en récupérant l'url au niveau du bouton
bleu **clone** : 

    $ git clone user@host:/path/to/git/project.git

Pour le mode http : 

    $ git clone http://localhost/myproject

## Travailler sur la modification actuelle

`add` pour ajouter des fichiers à modifier dans le $localrepo$, `mv` pour les
déplacer, `rm` pour les supprimer.

## Faire un checkup

`status`, `grep`, `log`, `show`

## Gérer ses versions en local avec $localrepo$

1. Enregistrer les modifications dans le $localrepo$ : `git commit`. 
2. Récupérer la version du $localrepo$ dans l'espace de travail : `git checkout [branch]`
3. Création d'une nouvelle branche et changement de branche

~~~~
git branch mabranche
git checkout mabranche
~~~~

## Gérer ses versions avec le serveur distant $remoterepo$

Pour pouvoir modifier les versions du serveur distant, il faut stocker sa clef
ssh publique sur le serveur. Pour gitlab, c'est à l'url : https://gitlab.com/profile/keys

1. Mettre à jour le $remoterepo$ : `git push`
2. Récupérer/Merger la version d'un $remoterepo$ en local : `git pull`

# Config

Le fichier de config est `~/.gitconfig` de la forme : 

~~~~
[user]
	email = user@mymail.com
	name = user
~~~~
