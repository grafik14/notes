
# Installation et administration :  SVN+apache

    $ sudo apt-get install apache2 libapache2-mod-svn subversion
    $ sudo a2enmod dav_svn


 Création du 1er utilisateur

    $ sudo htpasswd -cs /etc/apache2/dav_svn.passwd user1
	$ sudo chown www-data:www-data /etc/apache2/dav_svn.passwd
	$ sudo /etc/init.d/apache2 restart


 Ajout d'un autre utilisateur

    $ sudo htpasswd -s /etc/apache2/dav_svn.passwd user2
	$ sudo /etc/init.d/apache2 restart


 FIchier de configuration SVN 

~~~~
    /etc/apache2/mods-enabled/dav_svn.conf
    $ sudo /etc/init.d/apache2 restart
~~~~

 Création d'un projet

~~~~
    $ sudo svnadmin create /home/svn/myproject
    $ sudo chown -R www-data.www-data /home/svn/myproject
    $ svn import myproject_dir http://remotehost/svn/myproject -m 'Initial import for ...'
~~~~


 Suppression d'un projet

~~~~
    svn delete -m "message" http://remotehost/svn/myproject
~~~~



# Côté utilisateur

 Copie initiale d'un projet depuis un serveur

    $ svn checkout http://remotehost/svn/myproject

 Mise à jour de la version locale avec la version serveur

    $ svn update

 Synchronise la copie locale avec le serveur

    $ svn commit 

 Création d'un snapshot/tags pour de nouvelles releases

    $ svn mkdir tags
    $ svn copy trunk/ tags/new-release-v01
    $ svn commit -m "new release" tags

  Snapshot distant

    $ svn copy http://remotehost/svn/myproject http://remotehost/svn/myproject/tags/release-0.3 -m "Tagging the 0.3 release of the project."






